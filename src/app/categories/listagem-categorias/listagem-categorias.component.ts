import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ApiService } from '../../api.service';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-listagem-categorias',
    templateUrl: './listagem-categorias.component.html',
    styleUrls: ['./listagem-categorias.component.css']
})
export class ListagemCategoriasComponent implements OnInit {
    categorias: any[] = [];
    openForm: boolean = false;
    formName: FormGroup;

    constructor(private route: ActivatedRoute, private apiService: ApiService) { }

    ngOnInit() {
        this.listar();
    }

    listar(){
        this.apiService
            .listar('categories')
            .subscribe(cats => this.categorias = cats);
    }

    excluir(id: number) {
        this.apiService
            .excluir('categories/'+id)
            .subscribe(() => this.listar() );
    }

    toggle() {
        if (this.openForm)
            this.openForm = false;
        else
            this.openForm = true;
    }

    cadastrar() {
        const newCadastro = this.formName.getRawValue();

        this.apiService
            .gravar('categories', newCadastro)
            .subscribe(() => this.listar());
    }
}